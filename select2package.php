<?php
/**
 * select2package add a package select2 for public survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class select2package extends \ls\pluginmanager\PluginBase
{

  static protected $description = 'Use select2 on dropdown and helper for other plugin';
  static protected $name = 'select2package';


  public function init()
  {
    /* Register package : do it before other function : allow other plugin to use it*/
    Yii::setPathOfAlias('select2package',dirname(__FILE__));
    Yii::app()->clientScript->addPackage( 'select2', array(
        'basePath'    => 'select2package.vendor.select2',
        'css'         => array('css/select2.css'),
        'js'          => array('js/select2.js'),
        'depends'     => array('jquery')
    ));
    Yii::app()->clientScript->addPackage( 'select2-bootstrap-theme', array(
        'basePath'    => 'select2package.vendor.select2-bootstrap-theme',
        'css'         => array('select2-bootstrap.css'),
        'depends'     => array('bootstrap','select2')
    ));
    $this->subscribe('beforeSurveyPage');
  }

  /**
   * Needed only in survey
   */
  public function beforeSurveyPage()
  {

    Yii::app()->getClientScript()->registerPackage('select2-bootstrap-theme');
    $script="$('.dropdown-item select').select2({ theme: 'bootstrap', minimumResultsForSearch: 15 });";
    Yii::app()->getClientScript()->registerScript('select2package',$script,CClientScript::POS_READY);
  }
}
